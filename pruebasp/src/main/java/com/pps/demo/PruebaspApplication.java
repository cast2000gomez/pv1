package com.pps.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruebaspApplication {

	public static void main(String[] args) {
		SpringApplication.run(PruebaspApplication.class, args);
	}

}
